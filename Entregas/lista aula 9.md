Lista Aula 9 - Tick! Tack! <br>
Frederico Curti
___

- Diagrama
![alt text](./diagrama.png "Logo Title Text 1")
Foram usados os componentes TC - Timer Counter, RTC - Real time Clock, e a board XPLAINED PRO OLED da Atmel



- 1 Explique o funcionamento interno de um RTC.(como ele conta os dias/meses e anos gastando pouca energia). 

    O RTC funciona através de um oscilador de cristal com a frequencia de 2^15 Hz, que é conveniente para circuitos binários. Sua alimentação é isolada do sistema principal, podendo ser alimentado com pouca energia, normalmente de uma fonte externa como uma bateria de relógio, e continua funcionando mesmo quando o dispositivo está desligado.

- 2 Explique como o LINUX controla e acessa o RTC do computador

    Através do kernel, que fornece uma API em C para aplicações utilizarem o hardware

- 3 Como o Timer Counter pode ser utilizado para medir a velocidade e posição de um motor usando um encoder ótico?

    O Timer Counter é capaz de detectar e computar oscilações de um sinal, ou seja, é capaz de contar quantas oscilações foram geradas pelo encoder


- 4 Qual o consumo de energia do RTC no SAME70 ?

    1,1µA
