/**
 * \file
 *
 * \brief FreeRTOS Real Time Kernel example.
 *
 * Copyright (c) 2012-2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

/**
 * \mainpage FreeRTOS Real Time Kernel example
 *
 * \section Purpose
 *
 * The FreeRTOS example will help users how to use FreeRTOS in SAM boards.
 * This basic application shows how to create task and get information of
 * created task.
 *
 * \section Requirements
 *
 * This package can be used with SAM boards.
 *
 * \section Description
 *
 * The demonstration program create two task, one is make LED on the board
 * blink at a fixed rate, and another is monitor status of task.
 *
 * \section Usage
 *
 * -# Build the program and download it inside the evaluation board. Please
 *    refer to the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6224.pdf">
 *    SAM-BA User Guide</a>, the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6310.pdf">
 *    GNU-Based Software Development</a>
 *    application note or to the
 *    <a href="ftp://ftp.iar.se/WWWfiles/arm/Guides/EWARM_UserGuide.ENU.pdf">
 *    IAR EWARM User Guide</a>,
 *    depending on your chosen solution.
 * -# On the computer, open and configure a terminal application
 *    (e.g. HyperTerminal on Microsoft Windows) with these settings:
 *   - 115200 bauds
 *   - 8 bits of data
 *   - No parity
 *   - 1 stop bit
 *   - No flow control
 * -# Start the application.
 * -# LED should start blinking on the board. In the terminal window, the
 *    following text should appear (values depend on the board and chip used):
 *    \code
	-- Freertos Example xxx --
	-- xxxxxx-xx
	-- Compiled: xxx xx xxxx xx:xx:xx --
\endcode
 *
 */

#include <asf.h>
#include "conf_board.h"

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (4) // MAX PRIORITY - 1 



/* Botao da placa */
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

// Botoes da board externa
#define BUT1_PIO_ID				ID_PIOD
#define BUT1_PIO				PIOD
#define BUT1_PIN				28
#define BUT1_PIN_MASK			(1 << BUT1_PIN)

#define BUT2_PIO_ID				ID_PIOC
#define BUT2_PIO				PIOC
#define BUT2_PIN				31
#define BUT2_PIN_MASK			(1 << BUT2_PIN)

#define BUT3_PIO_ID				ID_PIOA
#define BUT3_PIO				PIOA
#define BUT3_PIN				19
#define BUT3_PIN_MASK			(1 << BUT3_PIN)

// LEDS da board externa
#define LED1_PIO_ID		ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID		ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID		ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		2
#define LED3_PIN_MASK   (1<<LED3_PIN)

// Prototypes
void but0_callback(void);
void but1_callback(void);
void but2_callback(void);
void but3_callback(void);
void init_but(Pio *pio, u_int32_t pio_id, u_int32_t pio_pin_mask, unsigned (*f)(void));
void toggle_led(Pio *pio, int led_pin_mask);
void shutdown_led(Pio *pio, int led_pin_mask);
void init_led(void);

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore0;
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;
SemaphoreHandle_t xSemaphore3;



#if !(SAMV71 || SAME70)
/**
 * \brief Handler for System Tick interrupt.
 */
void SysTick_Handler(void)
{
	xPortSysTickHandler();
}
#endif

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);	
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u\n", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */
static void task_led0(void *pvParameters)
{
	bool led_status = true;
	xSemaphore0 = xSemaphoreCreateBinary();
	const TickType_t xDelay = 100 / portTICK_PERIOD_MS;
	UNUSED(pvParameters);
	
	if (xSemaphore0 == NULL)
		printf("falha em criar o semaforo \n");
		
	for (;;) {
		if (xSemaphoreTake(xSemaphore0, (TickType_t) 0) == pdTRUE){
			printf("semaphore obtained\n");
			led_status = !led_status;
		} else {
			printf("expired \n");
			led_status ? LED_Toggle(LED0) : LED_Off(LED0); 
			vTaskDelay(xDelay);
		}

	}
}

static void task_led1(void *pvParameters)
{
	bool led_status = true;
	xSemaphore1 = xSemaphoreCreateBinary();
	const TickType_t xDelay = 250 / portTICK_PERIOD_MS;
	UNUSED(pvParameters);
	
	if (xSemaphore1 == NULL)
	printf("falha em criar o semaforo \n");
	
	for (;;) {
		if (xSemaphoreTake(xSemaphore1, (TickType_t) 0) == pdTRUE){
			printf("semaphore obtained\n");
			led_status = !led_status;
		} else {
			printf("expired \n");
			led_status ? toggle_led(LED1_PIO, LED1_PIN_MASK) : shutdown_led(LED1_PIO, LED1_PIN_MASK);
			vTaskDelay(xDelay);
		}
	}
}

static void task_led2(void *pvParameters)
{
	bool led_status = true;
	xSemaphore2 = xSemaphoreCreateBinary();
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	UNUSED(pvParameters);
	
	if (xSemaphore2 == NULL)
	printf("falha em criar o semaforo \n");
	
	for (;;) {
		if (xSemaphoreTake(xSemaphore2, (TickType_t) 0) == pdTRUE){
			printf("semaphore obtained\n");
			led_status = !led_status;
		} else {
			printf("expired \n");
			led_status ? toggle_led(LED2_PIO, LED2_PIN_MASK) : shutdown_led(LED2_PIO, LED2_PIN_MASK);
			vTaskDelay(xDelay);
		}
	}
}

static void task_led3(void *pvParameters)
{
	bool led_status = true;
	xSemaphore3 = xSemaphoreCreateBinary();
	const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;
	UNUSED(pvParameters);
	
	if (xSemaphore3 == NULL)
	printf("falha em criar o semaforo \n");
	
	for (;;) {
		if (xSemaphoreTake(xSemaphore3, (TickType_t) 0) == pdTRUE){
			printf("semaphore obtained\n");
			led_status = !led_status;
		} else {
			//printf("expired \n");
			led_status ? toggle_led(LED3_PIO, LED3_PIN_MASK) : shutdown_led(LED3_PIO, LED3_PIN_MASK);
			vTaskDelay(xDelay);
		}
	}
}

void but0_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore0, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void but1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void but2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void but3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore3, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void toggle_led(Pio *pio, int led_pin_mask){
	pio_toggle_pin_group(pio, led_pin_mask);
}

void shutdown_led(Pio *pio, int led_pin_mask){
	pio_set(pio, led_pin_mask);
}

void init_but(Pio *pio, u_int32_t pio_id, u_int32_t pio_pin_mask, unsigned (*f)(void)){
	// BUT_PIO_ID
	// BUT_PIO
	// BUT_PIO_PIN_MASK
	// CALLBACK
	/* conf bot�o como entrada */
	NVIC_EnableIRQ(pio_id);
	NVIC_SetPriority(pio_id, 8);
	pio_configure(pio, PIO_INPUT, pio_pin_mask, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(pio, pio_pin_mask, 60);
	pio_enable_interrupt(pio, pio_pin_mask);
	pio_handler_set(pio, pio_id, pio_pin_mask, PIO_IT_FALL_EDGE , f);
}

void init_led(){
	pmc_enable_periph_clk(LED3_PIO_ID);
	pmc_enable_periph_clk(LED2_PIO_ID);
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, 1, 0, 0);
	pio_set_output(LED3_PIO, LED3_PIN_MASK, 1, 0, 0);
};


/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	// inicializa leds
	init_led();

	/* iniciliza botao */
	init_but(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, &but0_callback);
	init_but(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, &but1_callback);
	init_but(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, &but2_callback);
	init_but(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, &but3_callback);

	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led0, "Led0", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}

	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
		
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
