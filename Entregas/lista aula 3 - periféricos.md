**Lista aula 3 - Periféricos**

Frederico Curti
___

- Pergunta 1

### RTC
  O RTC é um períferico de baixo consumo energético que combina um
  relógio com o horário do dia com alarmes e um calendário gregoriano
  ou persa, acoplado de um interruptor periódico programável.

### TC
  O TC (Timer Counter) é um módulo que inclui três canais de TC indênticos. Cada canal pode ser configurado para várias funcões,
  como medicão de frequência, contagem de eventos, medicao de intervalos, geracão de pulsos, atrasos e PWM.

### ISI
  O ISI (Image Sensor Interface) conecta um sensor de imagens CMOS ao processador e fornece captura de imagens em vários formatos. Ele promove a conversão de dados se necessária antes do armazenamento na memória através do DMA
  O Módulo também possui um FIFO interno com um caminho compativel com o controlador de LCD para uma prévia da imagem, além de conseguir escalar a imagem para ficar de acordo com a resolucão dos displays.

- Pergunta 2

  A memória reservada para os periféricos inicia no endereco 0x40000000
  e termina no endereco 0x60000000, logo seu tamanho é a diferença desses enderecos, ou seja, *536.870.912* bytes ~ 512MB

- Pergunta 3

| PERIFÉRICO    | Endereço      |
| ------------- |:-------------:|
| PIOA          | 0x400E0E00U   |
| PIOB          | 0x400E1000U   |
| ACC           | 0x40044000U   |
| UART1         | 0x400E0A00U   |
| UART2         | 0x400E1A00U   |

- Pergunta 4
   
   O id do TC0 é 23

- Pergunta 5

    O PC1 pode configurar o periférico D1 e o PWMC0_PWML1

    o PB6 pode fornecer um sinal de SWDIO/TMS mas não configura nenhum periférico 

- Pergunta 6

  Debouncing é um algoritmo que "aguarda" a manipulação de alguma informação que se altera com muita velocidade, mas a consequencia dessa alteração tem um custo computacional alto, logo se aguarda que esse valor se "estabilize" para tomar alguma decisão. Ele deve ser utilizado para obter uma leitura de algum sinal por exemplo, para ler o input que foi dado num teclado mecânico, que possui uma mola que provocará variações no contatos metálicos. Um algoritmo comum que implementa debouncing é nos navegadores, que aguardam o usuário terminar de redimensionar sua janela para então reorganizar os componentes do site para uma visualização otimizada ao novo tamanho.

