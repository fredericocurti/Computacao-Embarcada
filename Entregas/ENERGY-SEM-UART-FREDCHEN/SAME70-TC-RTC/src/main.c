#include "asf.h"
#include <string.h>
#include <stdio.h>
#include "conf_board.h"
#include "conf_clock.h"
#include "conf_uart_serial.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      1
#define SECOND      0

/**
* LEDs
*/
#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)

#define LED1_PIO_ID	   ID_PIOA
#define LED1_PIO       PIOA
#define LED1_PIN		   0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define USART_COM_ID ID_USART1
#define USART_COM    USART1


/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t rtc_triggered = 0;
volatile uint8_t flash_count = 0;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void LED_init(void);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void PWM_init(void);
void RTC_init(void);
static void USART1_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Handle Interrupcao botao 1
*/

static void configure_console(void){
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}




void USART1_Handler(void){
	uint32_t ret = usart_get_status(USART_COM);
	char c;

	// Verifica por qual motivo entrou na interrup�cao
	//  - Dado dispon�vel para leitura
	if(ret & US_IER_RXRDY){
		usart_serial_getchar(USART_COM, &c);
		pio_clear(LED1_PIO, LED1_PIN_MASK);
		//printf("obtained char %c\n",c);
		// -  Transmissoa finalizada
		} else if(ret & US_IER_TXRDY){

	}
}

/**
*  Interrupt handler for TC1 interrupt.
*/
void TC1_Handler(void) {
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	pin_toggle(LED1_PIO, LED1_PIN_MASK);		
	flash_count++;
}

/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/
void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);
	
	// get current time
	uint32_t h, m, s;
	rtc_get_time(RTC,&h,&m,&s);
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		rtc_triggered = 1;
		rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s + 2);
		pmc_enable_periph_clk(ID_TC1);
		pmc_enable_periph_clk(LED1_PIO_ID);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);	
}


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void PWM_init(void){
	pwm_channel_t pwm_channel_instance;
	pmc_enable_periph_clk(ID_PWM0);
	pwm_channel_disable(PWM0, PWM_CHANNEL_0);
	pwm_clock_t clock_setting = {
		.ul_clka = 1000 * 100,
		.ul_clkb = 0,
		.ul_mck  = 300000000,
	};
	pwm_channel_instance.ul_prescaler = PWM_CMR_CPRE_CLKA;
	pwm_channel_instance.ul_period = 100;
	pwm_channel_instance.ul_duty = 50;
	pwm_channel_instance.channel = PWM_CHANNEL_0;
	
	pwm_init(PWM0, &clock_setting);
	//pwm_channel_enable(PWM0, p)
}


/**
* @Brief Inicializa o pino do LED
*/
void LED_init(void){
	//pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(LED1_PIO_ID);
	
	//pio_set_output(LED1_PIO, LED1_PIN_MASK, )
	//pio_set_output(LED_PIO, LED_PIN_MASK, 0, 0, 0 );
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0 );
};

static void USART1_init(void){
	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4); // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate       = 115200,
		.char_length    = US_MR_CHRL_8_BIT,
		.parity_type    = US_MR_PAR_NO,
		.stop_bits   	= US_MR_NBSTOP_1_BIT	,
		.channel_mode   = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(USART_COM_ID);

	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART_COM);
	usart_enable_rx(USART_COM);

	/* map printf to usart */
	ptr_put = (int (*)(void volatile*,char))&usart_serial_putchar;
	ptr_get = (void (*)(void volatile*,char*))&usart_serial_getchar;

	/* ativando interrupcao */
	usart_enable_interrupt(USART_COM, US_IER_RXRDY);
	NVIC_SetPriority(USART_COM_ID, 4);
	NVIC_EnableIRQ(USART_COM_ID);
}



/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
	//pmc_disable_periph_clk(ID_TC);
}

/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);

}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	uint8_t am = 8;
	char t = 't';
	/* Initialize the SAM system */
	sysclk_init();
	board_init();
	
	configure_console();
		
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Disable all peripherals
	pmc_disable_all_periph_clk();

	/* Configura Leds */
	LED_init();

	/** Configura RTC */
	RTC_init();
	
	// usart
	//USART1_init();

	/** Configura timer TC0, canal 1 */
	TC_init(TC0, ID_TC1, 1, 30);
	
	// Disables backup SRAM
	SUPC->SUPC_MR = SUPC_MR_ONREG;
	
	/* Set wakeup source to rtt_alarm */
	pmc_set_fast_startup_input(PMC_FSMR_RTCAL);
	supc_set_wakeup_mode(SUPC, SUPC_WUMR_RTCEN_ENABLE);
	
	rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND + 2);
	
	//ptr_put(USART_COM, t);
	
	while (1) {
		// Enable backup mode
		/* Entrar em modo sleep bakcup*/
		if (rtc_triggered) {
			pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
			if (flash_count == am) {
				rtc_triggered = false;
				flash_count = 0;
				pmc_disable_periph_clk(ID_TC1);
			}
		} else {
			pmc_disable_periph_clk(LED1_PIO_ID);
			pmc_sleep(SAM_PM_SMODE_BACKUP);
		}
	}

}
