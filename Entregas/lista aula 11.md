Lista Aula 11 - RTOS <br>
Frederico Curti
___

- 1 O que é uma tarefa no sistema operacional.
 
 Uma tarefa é um programa que está em execução através de uma camada de abstração promovida pelo sistema operacional, que reveza o tempo de execução entre todas as tarefas criando a sensação de exclusividade da CPU e dos recursos para cada programa



- 2 De exemplos reais da utilização de RTOS HARD e SOFT.

Sistemas HARD são poucos pois são de ação absolutamente crítica, ou seja, atrasos não podem ser tolerados, como por exemplo aplicações de defesa militar, sistemas de controle de energia nuclear, aviação.
Sistemas SOFT por outro lado não possuem esse compromisso com o tempo de execução das tarefas, embora o desempenho dessa tarefa diminua ao longo do tempo se muitas execuções atrasarem. Um exemplo é um sistema de som, se alguns bits de uma música forem pulados não é um grande problema, dificilmente isso será percebido pelo consumidor, porém se muitos acontecerem ocorrerá uma distorção na música.


- 3 Liste exemplos de RTOS opensource e de outros proprietários

Open Source: FreeRTOS, Frosted, IntrOS, ChronOS, BRTOS <br>
Proprietários: embOS, EUROS, MicroC/OS, Nucleus RTOS, OS-9


- 4 Por que uma interrupção em um RTOS tem que ser resolvida o quanto antes e em um OS tipo windows não ?

Por que os RTOSes normalmente são aplicados em sistemas embarcados que envolvem monitoramento e processamento de dados em tempo real, nos quais uma resposta ou ação atrasada é uma falha que pode causar consequências catastróficas, e isso envolve lidar com as interrupções com prioridade máxima para que esse atraso seja mínimo. Em um OS como o windows, nada de ruim pode acontecer além de uma perda de desempenho momentâneo. A prioridade é que a experiencia e a compatibilidade e a flexibilidade sejam boas, e não realizar uma tarefa específica com uma latência absurdamente baixa.

- 5 Qual a função do estado blocked ?

O estado blocked serve para que uma task aguarde por um evento, uma queue ou um semáforo, enquanto não gasta processamento. Isso serve para otimização energética e ganho de performance caso uma task dependa de eventos ou ações externas para processar algo.

- 6 Pesquise como o escalonador do FreeRtos funciona e escreva um
pequeno texto explicando.

O escalonador é uma parte essencial do kernel que é responsável por decidir qual tarefa deve ser executada em um período de tempo particular. Sua norma de funcionamento é definida pelo algoritmo que analisa as prioridades de cada tarefa e as executa até a completude de uma função ou até a disponibilidade de outra tarefa de prioridade mais alta, que deverá ser executada imediatamente, e depois a tarefa original poderá ser retomada após a execução da tarefa de maior prioridade.