/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12

#define BUT0_PIO_ID				ID_PIOA
#define BUT0_PIO					PIOA
#define BUT0_PIN					11
#define BUT0_PIN_MASK			(1 << BUT0_PIN)
#define BUT0_DEBOUNCING_VALUE	79

#define BUT1_PIO_ID				ID_PIOD
#define BUT1_PIO				PIOD
#define BUT1_PIN				28
#define BUT1_PIN_MASK			(1 << BUT1_PIN)

#define BUT2_PIO_ID				ID_PIOC
#define BUT2_PIO				PIOC
#define BUT2_PIN				31
#define BUT2_PIN_MASK			(1 << BUT2_PIN)

#define BUT3_PIO_ID				ID_PIOA
#define BUT3_PIO				PIOA
#define BUT3_PIN				19
#define BUT3_PIN_MASK			(1 << BUT3_PIN)

#define LED1_PIO_ID		ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
"-- "BOARD_NAME " --"STRING_EOL	\
"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

// Proto
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);
void BTN_init(void);
void drawAlarm(void);
void clearDisplay(void);
void pin_toggle(Pio *pio, uint32_t mask);


// flags | globals
volatile int flagRTC = 0;

volatile int flagBTN0 = 1;
volatile int flagBTN1 = 1;
volatile int flagBTN2 = 1;
volatile int flagBTN3 = 1;

volatile int seconds = 0;
volatile int hours = 12;
volatile int minutes = 15;
volatile int alarm_minutes = 1;
volatile int alarm_timeout = 5;
volatile int alarm_triggered = 0;
volatile int alarm_set = 0;

volatile int next_alarm_minutes = 0;
volatile int next_alarm_hours = 0;

volatile char mode = 'd';

// code

static void Button0_Handler(uint32_t id, uint32_t mask)
{
	flagBTN0 = !flagBTN0;
}


static void Button1_Handler(uint32_t id, uint32_t mask)
{
	flagBTN1 = !flagBTN1;
}

static void Button2_Handler(uint32_t id, uint32_t mask)
{
	flagBTN2 = !flagBTN2;
}

static void Button3_Handler(uint32_t id, uint32_t mask)
{
	flagBTN3 = !flagBTN3;
}

static void configure_console(void) {
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
		pio_clear(pio, mask);
	else
		pio_set(pio,mask);
}

void RTC_Handler(void) {
	uint32_t ul_status = rtc_get_status(RTC);
	
	// get current time
	uint32_t h, m, s;
	rtc_get_time(RTC,&h,&m,&s);
	
	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		flagRTC = 1;
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		alarm_triggered = 1;
		alarm_set = 0;
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}

void BTN_init(){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT0_PIO_ID);
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pmc_enable_periph_clk(BUT3_PIO_ID);
	
	pio_set_input(BUT0_PIO, BUT0_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
		

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT0_PIO, BUT0_PIN_MASK);
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
		
	pio_handler_set(BUT0_PIO, BUT0_PIO_ID, BUT0_PIN_MASK, PIO_IT_FALL_EDGE, Button0_Handler);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);
		

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT0_PIO_ID);
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_EnableIRQ(BUT3_PIO_ID);
		
	NVIC_SetPriority(BUT0_PIO_ID, 1);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
}

void LED_init(){
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0);
};

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, hours, minutes, seconds);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);
}

void clearDisplay(void){
	gfx_mono_draw_filled_rect(0, 0, 128, 32, GFX_PIXEL_CLR);
}

void drawAlarm(void){
	clearDisplay();
	char buffer[256];
	sprintf(buffer, "Alarm:%d", alarm_minutes);
	gfx_mono_draw_string(buffer, 0, 0, &sysfont);
}

void redraw(void) {
	clearDisplay();
	char buffer[256];
	sprintf(buffer, "%d:%d", hours, minutes);
	gfx_mono_draw_string(buffer, 0, 0, &sysfont);
	if (alarm_set){
		gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);	
	}
}

void showCurrentAlarm(){
	clearDisplay();
	char buffer[256];
	sprintf(buffer, "CA%d:%d", next_alarm_hours, next_alarm_minutes);	
	gfx_mono_draw_string(buffer, 0, 0, &sysfont);
	delay_ms(2000);
}

int main (void) {
	char buffer[256];
	sprintf(buffer, "%d:%d", hours, minutes);

	board_init();
	sysclk_init();
	delay_init();
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/** Configura perifericos */
	RTC_init();
	BTN_init();
	LED_init();
	gfx_mono_ssd1306_init();
	configure_console();
	
	printf("Im working\n");
	
    gfx_mono_draw_string(buffer, 0, 0, &sysfont);
	
	while(1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		
		if (flagRTC) {
			seconds++;
			printf("time: %d:%d:%d\n",hours, minutes, seconds);
			
			if (mode == 'a'){
				alarm_timeout--;
				printf("Alarm timeout: %d\n", alarm_timeout);
				if (alarm_timeout == 0){
					printf("alarm set timed out\n");
					mode = 'd';
					alarm_timeout = 5;
					redraw();
				}
			}
			
			if (alarm_triggered) {
				pin_toggle(LED1_PIO, LED1_PIN_MASK);
			}
			
			if (seconds == 60) {
				minutes++;
				seconds = 0;
				if (mode == 'd')
					redraw();
	
			}
			
			if (minutes == 60) {
				hours++;
				minutes = 0;
				if (mode == 'd')
					redraw();
			}
			
			flagRTC = 0;
		}
		
		if (mode == 'd' && flagBTN3 && alarm_set) {
			flagBTN3 = 0;
			showCurrentAlarm();
			redraw();
		}
		
		if (flagBTN1) {
			mode = 'a';
			alarm_timeout = 5;
			alarm_minutes = 1;
			drawAlarm();
			flagBTN1 = 0;
		}
		
		if (flagBTN3 && mode == 'a') {
			flagBTN3 = 0;
			alarm_minutes++;
			drawAlarm();
			alarm_timeout = 5;
		}
		
		if (flagBTN2 && mode == 'a') {
			rtc_set_time_alarm(RTC, 1, hours, 1, minutes + alarm_minutes, 1, seconds);
			printf("Alarm SET for %d minutes from now \n", alarm_minutes);
			flagBTN2 = 0;
			alarm_timeout = 5;
			alarm_set = 1;
			next_alarm_minutes = minutes + alarm_minutes;
			next_alarm_hours = hours;
			
			if (next_alarm_minutes > 60) {
				next_alarm_minutes = next_alarm_minutes - 60;
				next_alarm_hours++;
			}
			
			mode = 'd';
			redraw();
		}
				
		
		if (flagBTN0) {
			printf("Alarm disabled! \n");
			alarm_triggered = 0;
			pio_set(LED1_PIO, LED1_PIN_MASK);
			flagBTN0 = 0;
		}
		
	}
}
