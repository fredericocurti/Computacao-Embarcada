Lista Aula 6 <br>
Frederico Curti
___
- 1 Qual a diferença entre as exceções NMI e IRQ ?
   
    Elas lidam com pinos diferentes na CPU, e suas interrupções são lidadas de maneira difernte. No NMI, a interrupção é disparada pela borda do sinal, e a instrução da interrupção é executada após o fim dessa operação. No IRQ, a interrupção é disparada pelo nível do sinal, que se estiver em alta e a flag que disabilita a interrupção não estiver ativada, a rotina será executada.

- 2 Qual a diferença entre as exceções IRQ e ISR ?

    O ISR é um processo que acontece em software, que lida com o codigo que deve ser executado caso haja a interrupção, enquanto o IRQ acontece a nível de hardware e é responsável pela possibilidade das interrupções acontecerem. 

- 3 No ARM que utilizamos no curso, quantas são as interrupções suportadas e qual a sua menor prioridade ?

	Existem 256 prioridades (8 bits), onde 0 é a maior e 255 é a menor

- 4 Descreva o uso do FIQ.

	O FIQ é uma solitiação de interrupção de maior prioridade, que disabilita os handlers de outros IRQs, ou seja, nenhuma outra interrupção pode ocorrer durante o processamento do FIQ ativo.

- 5 IRQ vs FIQ No diagrama anterior, quem possui maior prioridade IRQ ou FIQ ?
	
    O FIQ.

- 6 No datasheet, secção 13.1 informa o ID do periférico que está associado com a sua interrupção. Busque a informação e liste o ID dos seguintes periféricos : 

• PIOA: ID 10 • PIOC: ID 12 • TC0: ID 23

- 7 O que acontece se não limparmos a interrupção (Ack) ?

    A interrupção continuará ativa e o programa vai continuamente executar a instrução associada à essa interrupção. 

- 8 Com base no texto anterior e nos diagramas de blocos descreva o uso da interrupção e suas opções.

A interrupção é ativada através da ativação de certos registradores, como o PIO_AIMER[0], e ela pode ser ativada por eventos como a subida ou a descida do sinal que está sendo monitorado, além do próprio nível deste (alta ou baixa).


- 9 Registradores Interrupção Descreva as funções dos registradores : 𝑃𝐼𝑂_𝐸𝐿𝑆𝑅 e PIO_FRLHSR

O PIO_ELSR é um registrador que só permite leitura, e indica 0 se a origem da interrupção é um detector de bordas ou 1 se a origem é um detector de nível

- 10 Faça um diagrama que sintetize como a interrupção funciona em um microcontrolador.
--